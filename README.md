# Serial_Cpp
Cruedly stitched togeather high-level library for sending and recieveng bytes over serial (usb) using modern c++. Works on Windows and Linux too!

## example
```cpp
#include "serial_cpp/Serial.hpp"
#include <iostream>
#include <algorithm>

int main(){
    Serial usb_ = Serial("/dev/ttyACM0");

    usb_.baudrate() = 9600; //default

    usb_.open();

    std::list<uint8_t> bytes_to_send = {0x00, 0x01, 0x02, 0x03}; // why list? - cuz I like std list!

    usb_.write(bytes_to_send); // write bytes to serial

    std::list<uint8_t> bytes_red = usb_.read(); // read bytes from serial

    std::for_each(bytes_red.begin(), bytes_red.end(), [](const auto &n) { std::cout << n << std::endl; }); // print bytes to cmd

    usb_.close();
}
```
