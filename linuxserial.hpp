#include <string>
#include <stdexcept>
#include <list>

#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()


#include "SerialTemplate.hpp"

//https://blog.mbedded.ninja/programming/operating-systems/linux/linux-serial-ports-using-c-cpp/#basic-setup-in-c

class Serial : public SerialTemplate{
public:
    using SerialTemplate::SerialTemplate;

    void open();

    void write(char bytes[], int len);
    
    void write(std::list<uint8_t> bytes){
        SerialTemplate::write(bytes);
    }
    std::list<uint8_t> read();

    void close();

private:
    int serial_port;

};