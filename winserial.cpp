#if _WIN32
    #include "winserial.hpp"


    void Serial::open(){
        DCB dcbSerialParams = {0};
        COMMTIMEOUTS timeouts = {0};
            


        std::string port_modified = "\\\\.\\" + port_;


        hSerial = CreateFile(port_modified.c_str(), GENERIC_READ|GENERIC_WRITE, 0, NULL,
                OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL ); 
        if (hSerial == INVALID_HANDLE_VALUE)
        {
                throw std::invalid_argument("Error while opening serial port!");
        }

        dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
        if (GetCommState(hSerial, &dcbSerialParams) == 0){
            CloseHandle(hSerial);
            throw std::invalid_argument("Error getting device state!");
        }
        
        dcbSerialParams.BaudRate = baudrate_;
        dcbSerialParams.ByteSize = byte_size_;

        auto parsed_stop_bits = ONESTOPBIT;

        if(stop_bits_ == 1) parsed_stop_bits = ONESTOPBIT;
        else if(stop_bits_ == 1.5) parsed_stop_bits = ONE5STOPBITS;
        else if(stop_bits_ == 2) parsed_stop_bits = TWOSTOPBITS;
        else{
            throw std::invalid_argument("wrong stopbit value!");
        }

        dcbSerialParams.StopBits = parsed_stop_bits;
        dcbSerialParams.Parity = NOPARITY;
        if(SetCommState(hSerial, &dcbSerialParams) == 0)
        {
            CloseHandle(hSerial);
            throw std::invalid_argument("Error setting device parameters!");
        }

        // Set COM port timeout settings
        timeouts.ReadIntervalTimeout = 50;
        timeouts.ReadTotalTimeoutConstant = 50;
        timeouts.ReadTotalTimeoutMultiplier = 10;
        timeouts.WriteTotalTimeoutConstant = 50;
        timeouts.WriteTotalTimeoutMultiplier = 10;
        if(SetCommTimeouts(hSerial, &timeouts) == 0)
        {
            CloseHandle(hSerial);
            throw std::invalid_argument("Error setting device parameters!");
        }

    }

    void Serial::write(char bytes[], int len){
        DWORD bytes_written, total_bytes_written = 0;
        fprintf(stderr, "Sending bytes...");
        if(!WriteFile(hSerial, bytes, 6, &bytes_written, NULL))
        {
            CloseHandle(hSerial);
            throw std::invalid_argument("Error while Sending bytes!");
        }   
        fprintf(stderr, "%d bytes written\n", bytes_written);
    }


    void Serial::close(){
        if (CloseHandle(hSerial) == 0)
        {
            throw std::invalid_argument("Error while Closing serial port!");

        }
    }

    std::list<uint8_t> Serial::read(){
        uint8_t bytes_red[100];
        LPDWORD size_of_red_data;

        std::list<uint8_t> ret;

        if(!ReadFile(hSerial, bytes_red, 100, size_of_red_data, NULL))
        {
            CloseHandle(hSerial);
            throw std::invalid_argument("Error while reading bytes!");
        }
        for(unsigned int i =0; i< *size_of_red_data; ++i){
            ret.push_back(bytes_red[i]);

        }
        return ret;
    }

#endif