#if __linux__
    #include "linuxserial.hpp"

    #include <cstring>
    #include <map>



    void Serial::open(){

        serial_port = ::open(port_.c_str(), O_RDWR);
        // Check for errors
        if (serial_port < 0) {
            printf("Error %i from open: %s\n", errno, std::strerror(errno));
        }

        struct termios tty;

        if(tcgetattr(serial_port, &tty) != 0) {
            printf("Error %i from tcgetattr: %s\n", errno, std::strerror(errno));
        }

        tty.c_cflag &= ~PARENB; // Clear parity bit, disabling parity (most common)


        //STOPBIT

        if(stop_bits_ != 1){
            throw std::invalid_argument("stop bits " + std::to_string(stop_bits_) + " not supported under Linux!");
        }

        //TODO - are no other stop bits supported?

        tty.c_cflag &= ~CSTOPB; // Clear stop field, only one stop bit used in communication (most common)


        // BITS PER BYTE - bytesize

        std::map<short unsigned int, int> bpb_m{
            {5, CS5},
            {6, CS6},
            {7, CS7},
            {8, CS8},

        };

        if (auto search = bpb_m.find(byte_size_); search == bpb_m.end()){
            throw std::invalid_argument("byte size " + std::to_string(byte_size_) + " not supported under Linux!");
        }


        tty.c_cflag |= bpb_m[byte_size_];


        tty.c_cflag &= ~CRTSCTS; // Disable RTS/CTS hardware flow control (most common)

        //BAUDRATE

        std::map<unsigned int, int> baudrate_m{
            {0, B0},
            {50, B50},
            {75, B75},
            {110, B110},
            {134, B134},
            {150, B150},
            {200, B200},
            {300, B300},
            {600, B600},
            {1200, B1200},
            {1800, B1800},
            {2400, B2400},
            {4800, B4800},
            {9600, B9600},
            {19200, B19200},
            {38400, B38400},
        };

        if (auto search = baudrate_m.find(baudrate_); search == baudrate_m.end()){
            throw std::invalid_argument("baudrate " + std::to_string(baudrate_) + " not supported under Linux!");
        }

        cfsetispeed(&tty, baudrate_m[baudrate_]);
        cfsetospeed(&tty, baudrate_m[baudrate_]);

        // Save tty settings, also checking for error
        if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
            printf("Error %i from tcsetattr: %s\n", errno, std::strerror(errno));
        }

        printf("opened succesfully!");


    }

    void Serial::write(char bytes[], int len){
        ::write(serial_port, bytes, len);
        usleep ((len + 25) * 100);  // TODO - why 25 and make dependent on the baudrate?
    }

    std::list<uint8_t> Serial::read(){
        char read_buf [256];

        std::list<uint8_t> ret;

        int n = ::read(serial_port, &read_buf, sizeof(read_buf));

        printf("\ndlugosc odp:%d \n", n);

        for(int i =0; i< n; ++i){
            ret.push_back(static_cast<uint8_t>(read_buf[i]));
        }

        return ret;
    }

    void Serial::close(){
        ::close(serial_port);
    }

#endif