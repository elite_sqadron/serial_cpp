#include <string>
#include <list>

class SerialTemplate{
public:
    SerialTemplate(std::string port) : port_(port), baudrate_(9600), byte_size_(8), stop_bits_(1){}
    SerialTemplate() : SerialTemplate("/dev/ttyACM0") {}
    virtual void open() = 0;

    virtual void write(char bytes[], int len) = 0;

    void write(std::list<uint8_t> bytes){
        char rewritten_bytes[bytes.size()];
        std::copy(bytes.begin(), bytes.end(), rewritten_bytes);
        write(rewritten_bytes, bytes.size());
    }

    virtual std::list<uint8_t> read() = 0;

    virtual void close() = 0;


    unsigned int& baudrate() {return baudrate_;}
    const unsigned int& baudrate() const {return baudrate_;}

    unsigned short int& byte_size() {return byte_size_;}
    const unsigned short int& byte_size() const {return byte_size_;}

    float& stop_bits() {return stop_bits_;}
    const float& stop_bits() const {return stop_bits_;}


protected:
    std::string port_;

    unsigned int baudrate_;
    unsigned short int byte_size_;
    float stop_bits_;

    //TODO - parity bit
    //TODO - timeouts
};