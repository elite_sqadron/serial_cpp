#include <windows.h>
#include <stdio.h>

#include <string>
#include <stdexcept>
#include <list>

#include "SerialTemplate.hpp"

//https://batchloaf.wordpress.com/2013/02/13/writing-bytes-to-a-serial-port-in-c/

class Serial : public SerialTemplate{
public:
    using SerialTemplate::SerialTemplate;
    void open();

    void write(char bytes[], int len);

    void write(std::list<uint8_t> bytes){
        SerialTemplate::write(bytes);
    }

    std::list<uint8_t> read();

    void close();
private:
    void* hSerial;
};